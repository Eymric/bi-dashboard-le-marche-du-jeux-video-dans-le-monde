# Module Business Intelligence 
## Dashboard sur le marché des jeux vidéos

[Lien vers la certification Google Data Studio](https://analytics.google.com/analytics/academy/certificate/pBNNVZ-eQKuV0oHisClSsw)

[Lien vers le dashboard](https://datastudio.google.com/s/t60DG4iuV50)

* Comment avez vous choisi la source de données?

Pour choisir ma source de données, je me suis rendu sur le moteur de recherche de dataset que nous offre Google ([lien](https://datasetsearch.research.google.com/)), avec le filtre "Gratuit". 
Dans un premier temps, j'avais choisi l'énorme jeux de données que nous offre le site OpenData sur la ville de Paris. Cependant, suite à une importation des données échouées (dû aux points virgules dans le CSV à remplacer par une virgule),
j'ai choisi de me réorienté sur un domaine qui m'intéresse davantage: Les jeux vidéos. Grâce au moteur de recherche, j'ai pu récupérer sur la plateforme [Kaggle](https://www.kaggle.com/) un dataset de plus de 16.500 jeux vidéos vendus dans le monde.


* Explication détaillées pas-à-pas de comment vous avez injecter les
  données dans Google Data Studio.

Une fois mon rapport crée, il suffit de cliquer sur l'onglet 'Ajouter des données'.
![alt dashboard](/dashboard.png)
Ensuite choisir le connecteur, étant donné qu'il s'agit d'un fichier .CSV, cliquer sur la box 'Importation de fichiers'. 
![alt dashboard](/dashboard1.png)
J'ai ensuite importer mon fichier csv, une fois l'importation fini, il suffit de cliquer sur le bouton "Ajouter" en bas à droite de la page.
![alt dashboard](/dashboard2.png)
Une fenêtre modal apparait sur l'écran, il faut cliquer une nouvelle fois sur le bouton "Ajouter au rapport". 
![alt dashboard](/dashboard3.png)
Les données sont désormais injecter dans notre rapport Google Data Studio.

* Compte-rendu détaillé des ressources pédagogiques supplémentaires
  utilisées pour mener à bien votre projet.
  
Pour mener à bien mon projet, j'ai tout d'abord commencer par la réalisation d'un tutoriel vidéo sur YouTube : [Comment créer un Dashboard Data Studio (de A à Z)](https://www.youtube.com/watch?v=4xmyomDMnnc&feature=youtu.be). Avec l'aide d'un deuxième écran, j'ai réalisé exactement le même Dashboard que la vidéo, que l'on peut retrouver sur le lien [ici](https://datastudio.google.com/s/tgqWhOSA058).
Cette vidéo m'a permis d'apprendre les bases, nottament pour les tableaux de données, la configuration de la plage de date avancées. Les graphiques combinées et leurs cohérences avec les plages de date. La possibilité d'ajouter des barres dans les tableaux ou les cartes de densité, ainsi que la façon de styliser son dashboard. D'ailleurs on peut remarquer au niveau du style que mon Dashboard a légèrement (beaucoup) été inspiré de celui-ci.
Par la suite, ne sachant pas trop par quoi commencer, j'ai essayé les templates fourni par Google Data Studio, j'ai pu essayé le template pour Youtube Analytics. J'ai voulu essayé avec mon compte Youtube, mais n'étant pas un youtubeur, il n'y avait peu, voir zéro données... C'est par la suite que j'ai choisi de chercher des datasets sur Google.
Durant la réalisation de mon Dashboard, je me suis appuyé sur une magnifique ressource qui est [extreme Presentation Chart Chooser](https://extremepresentation.typepad.com/.a/6a00d8341bfd2e53ef0263ec2ab026200c-pi), Selon ce que l'on veut montrer sur notre Dashboard, il nous indique le graph le plus approprié. 
Je me suis aussi servi de [coolors](https://coolors.co/), pour avoir une palette de couleur "cohérente" pour le tableau de donnée présent dans mon dashboard.
